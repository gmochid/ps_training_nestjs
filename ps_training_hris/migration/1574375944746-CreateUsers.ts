import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateUsers1574375944746 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.createTable(new Table({
            name: 'users',
            columns: [
                {
                    name: 'id',
                    type: 'int',
                    isPrimary: true,
                },
                {
                    name: 'username',
                    type: 'string',
                },
                {
                    name: 'password',
                    type: 'string',
                },
                {
                    name: 'name',
                    type: 'string',
                }
            ]
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

import { Request } from "express"
export interface HRISRequest extends Request {
  user: object,
  logout: Function // or any other type
}

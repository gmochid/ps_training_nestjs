// src/app.controller.ts
import {
  Controller,
  Get,
  Post,
  Req,
  Res,
  Render,
  UseGuards,
  UseFilters,
  Param,
  Query,
} from '@nestjs/common';
import { Response } from 'express';

import { LoginGuard } from './common/guards/login.guard';
import { AuthenticatedGuard } from './common/guards/authenticated.guard';
import { AuthExceptionFilter } from './common/filters/auth-exceptions.filter';
import { HRISRequest } from './typings/request';
import { UsersService } from './modules/users/users.service';

@Controller()
@UseFilters(AuthExceptionFilter)
export class AppController {

  constructor(private readonly usersService: UsersService) {}

  @Get('/')
  @Render('login')
  index(@Req() req: HRISRequest): { message: string } {
    return { message: req.flash('loginError') };
  }

  @UseGuards(LoginGuard)
  @Post('/login')
  login(@Res() res: Response) {
    res.redirect('/home');
  }

  @UseGuards(AuthenticatedGuard)
  @Get('/home')
  @Render('home')
  getHome(@Req() req: HRISRequest) {
    return { user: req.user };
  }

  @Get('/hello')
  @Render('hello')
  getHello() {
    return {
      title: 'Ini hello'
    };
  }

  @Get('/hello/user')
  @Render('hello-user')
  getHelloUser() {
    return {
      users: this.usersService.findAll()
    };
  }

  @Get('/hello/user/:username')
  getFindUser(@Param('username') username: string, @Query('id') id: number) {
    return {
      status: true,
      error: null,
      data: {
        user: this.usersService.findOne(username)
      }
    }
  }

  @UseGuards(AuthenticatedGuard)
  @Get('/profile')
  @Render('profile')
  getProfile(@Req() req: HRISRequest) {
    return { user: req.user };
  }

  @Get('/logout')
  logout(@Req() req: HRISRequest, @Res() res: Response) {
    req.logout();
    res.redirect('/');
  }
}
